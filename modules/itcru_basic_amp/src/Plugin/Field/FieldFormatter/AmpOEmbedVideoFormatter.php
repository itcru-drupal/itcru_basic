<?php

namespace Drupal\itcru_basic_amp\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media\OEmbed\Resource;
use Drupal\media\OEmbed\ResourceException;
use Drupal\media\Plugin\Field\FieldFormatter\OEmbedFormatter;

/**
 * Plugin implementation of the AMP remote video formatter.
 *
 * @FieldFormatter(
 *   id = "amp_oembed_video",
 *   label = @Translation("AMP for oEmbed Video"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class AmpOEmbedVideoFormatter extends OEmbedFormatter { // @phpstan-ignore-line

  /**
   * The resource provider.
   *
   * @var \Drupal\media\OEmbed\Provider
   */
  protected $provider;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $main_property = $item->getFieldDefinition()->getFieldStorageDefinition()->getMainPropertyName();
      $value = $item->{$main_property};

      if (empty($value)) {
        continue;
      }

      $resource_url = $this->urlResolver->getResourceUrl($value);
      try {
        $resource = $this->resourceFetcher->fetchResource($resource_url);
      }
      catch (ResourceException $exception) {
        $this->logger->error("Could not retrieve the remote URL (@url).", ['@url' => $resource_url]);
        continue;
      }

      if ($resource->getType() === Resource::TYPE_VIDEO
        && $this->provider = $resource->getProvider()) {

        if ($this->isProviderCompatible()
          && $provider_video_id = $this->getIdFromInput($value)) {

          $provider_name = strtolower($this->provider->getName());
          $elements[$delta]['#attached']['library'] = [$this->getAmpLibrary()];
          $elements[$delta]['#theme'] = 'amp_oembed_video';
          $elements[$delta]['#provider'] = $provider_name;
          $elements[$delta]['#attributes'] = [
            'class' => [
              'video',
            ],
            'width' => '854',
            'height' => '480',
            'layout' => 'responsive',
            'data-videoid' => $provider_video_id,
          ];
        }
        else {
          $element[$delta] = [
            '#theme' => 'amp_oembed_video_not_compatible_provider',
          ];
        }
      }
    }
    return $elements;
  }

  /**
   * Return provider specific AMP library.
   *
   * @return string
   *   Return provider related AMP library as a string.
   */
  protected function getAmpLibrary() {
    return 'itcru_basic_amp/amp.' . strtolower($this->provider->getName());
  }

  /**
   * Get video ID from video URL string.
   *
   * @param string $input
   *   Url of embedded video.
   *
   * @return string|bool
   *   Return video ID of embedded video or FALSE.
   */
  protected function getIdFromInput($input) {
    $pattern = '/^https?:\/\/(www\.)?((?!.*list=)youtube\.com\/watch\?.*v=|youtu\.be\/)(?<id>[0-9A-Za-z_-]*)/';
    if ($this->provider->getName() == 'Vimeo') {
      $pattern = '\'/^https?:\/\/(www\.)?vimeo.com\/(channels\/[a-zA-Z0-9]*\/)?(?<id>[0-9]*)(\/[a-zA-Z0-9]+)?(\#t=(\d+)s)?$/\'';
    }
    preg_match($pattern, $input, $matches);
    return $matches['id'] ?? FALSE;
  }

  /**
   * Check if given provider is AMP compatible.
   *
   * @return bool
   *   Return TRUE if video provider is supported.
   */
  protected function isProviderCompatible() {
    return in_array($this->provider->getName(), ['Vimeo', 'YouTube']);
  }

}
