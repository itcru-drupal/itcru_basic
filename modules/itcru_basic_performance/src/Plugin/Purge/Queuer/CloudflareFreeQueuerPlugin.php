<?php

namespace Drupal\itcru_basic_performance\Plugin\Purge\Queuer;

use Drupal\purge\Plugin\Purge\Queuer\QueuerInterface;
use Drupal\purge\Plugin\Purge\Queuer\QueuerBase;

/**
 * Queues URLs to be purged.
 *
 * @PurgeQueuer(
 *   id = "cloudflare_free_queuer",
 *   label = @Translation("Cloudflare Free queuer"),
 *   description = @Translation("Queues URLs to be purged."),
 *   enable_by_default = true,
 * )
 */
class CloudflareFreeQueuerPlugin extends QueuerBase implements QueuerInterface {

}
