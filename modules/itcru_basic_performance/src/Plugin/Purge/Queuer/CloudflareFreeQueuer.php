<?php

namespace Drupal\itcru_basic_performance\Plugin\Purge\Queuer;

/**
 * Provide a purge queuer for Cloudflare.
 *
 * @package Drupal\itcru_basic_performance\Plugin\Purge\Queue
 */
class CloudflareFreeQueuer {

  /**
   * The purge invalidation service.
   *
   * @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface
   */
  public $purgeInvalidationFactory;

  /**
   * The purge queue service.
   *
   * @var \Drupal\purge\Plugin\Purge\Queue\QueueService
   */
  public $purgeQueue;

  /**
   * The querer service interface to initialize.
   *
   * @var \Drupal\purge\Plugin\Purge\Queuer\QueuerInterface|null
   */
  public $queuer;

  /**
   * Initialize purge queuer.
   *
   * @return bool
   *   Return TRUE or FALSE if purge queuer is initialized or not.
   */
  public function initialize() {
    if (is_null($this->queuer)) {
      $this->queuer = \Drupal::service('purge.queuers')->get('cloudflare_free_queuer');
      if ($this->queuer !== FALSE) {
        $this->purgeInvalidationFactory = \Drupal::service('purge.invalidation.factory');
        $this->purgeQueue = \Drupal::service('purge.queue');
      }
    }
    return $this->queuer !== FALSE;
  }

}
