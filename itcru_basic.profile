<?php

/**
 * @file
 * Enables modules and site configuration for a itcru_basic site installation.
 */

use Drupal\Component\Utility\Html;
use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function itcru_basic_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  $form['#submit'][] = 'itcru_basic_form_install_configure_submit';
}

/**
 * Submission handler to sync the contact.form.feedback recipient.
 */
function itcru_basic_form_install_configure_submit($form, FormStateInterface $form_state) {
  $site_mail = $form_state->getValue('site_mail');
  ContactForm::load('feedback')->setRecipients([$site_mail])->trustData()->save();
}

/**
 * Implements hook_ENTITY_TYPE_create() for files to clean up file names.
 */
function itcru_basic_file_create(FileInterface $file) {
  if (\Drupal::moduleHandler()->moduleExists('pathauto')) {

    /** @var \Drupal\pathauto\AliasCleaner $aliasCleaner */
    $aliasCleaner = \Drupal::service('pathauto.alias_cleaner');
    $cleanFilename = $aliasCleaner->cleanString(pathinfo($file->getFilename(), PATHINFO_FILENAME) . '.' . pathinfo($file->getFilename(), PATHINFO_EXTENSION));
    $file->setFilename($cleanFilename);
  }
}

/**
 * Implements hook_metatags_alter().
 */
function itcru_basic_metatags_alter(array &$metatags, array &$context): void {
  if (\Drupal::moduleHandler()->moduleExists('schema_qa_page')
    && empty($metatags['schema_qa_page_type'])) {
    itcru_basic_get_schema_qa_page($metatags, $context);
  }
}

/**
 * Helper function to extract schema_qa_page data
 * from bp_simple paragraphs on a node.
 *
 * @param array $metatags
 * @param array $context
 *
 * @return void
 */
function itcru_basic_get_schema_qa_page(array &$metatags, array &$context): void {
  if (isset($context['entity'])) {
    $entity = $context['entity'];

    if ($entity instanceof ContentEntityInterface
      && ($entity->hasField('field_paragraphs'))
      && !$entity->get('field_paragraphs')->isEmpty()) {

      /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $paragraphs */
      $paragraphs = $entity->get('field_paragraphs');

      /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
      foreach ($paragraphs->referencedEntities() as $paragraph) {
        if ($paragraph->bundle() == 'bp_simple'
          && !$paragraph->get('bp_header')->isEmpty()
          && !$paragraph->get('bp_text')->isEmpty()) {

          $question = $paragraph->get('bp_header')->value;
          if (str_ends_with($question, '?')) {
            $answer_text = $paragraph->get('bp_text')->value;
            $answer_url = sprintf('[%s:url:absolute]#%s', $entity->getEntityTypeId(), strtolower(Html::cleanCssIdentifier($paragraph->get('bp_header')->value)));

            $faqs[] = [
              '@type' => 'Question',
              'name' => $question,
              'acceptedAnswer' => [
                '@type' => 'Answer',
                'text' => $answer_text,
                'url' => $answer_url,
              ],
            ];
          }
        }
      }

      if (isset($faqs)) {
        $metatags['schema_qa_page_type'] = 'FAQPage';
        $metatags['schema_qa_page_main_entity'] = count($faqs) == 1 ? $faqs[0] : $faqs;
      }
    }
  }
}
